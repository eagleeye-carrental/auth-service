package com.eagleeye.authservice.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class RoleDto implements Serializable {
    private String name;
}
