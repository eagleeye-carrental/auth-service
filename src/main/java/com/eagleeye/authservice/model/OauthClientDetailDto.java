package com.eagleeye.authservice.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class OauthClientDetailDto implements Serializable {
    private String clientId;
    private String clientSecret;
    private String webServerRedirectUri;
    private String scope;
    private int accessTokenValidity;
    private int refreshTokenValidity;
    private String resourceIds;
    private String authorizedGrantTypes;
    private String authorities;
    private String autoApprove;
    private String verifyCode;
}
