//package com.eagleeye.authservice.configuration;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Primary;
//import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
//import org.springframework.security.oauth2.provider.token.TokenEnhancer;
//import org.springframework.security.oauth2.provider.token.TokenStore;
//import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
//import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
//
//@Configuration
//public class JwtTokenStoreConfig {
//
//    private String privateKey = "-----BEGIN RSA PRIVATE KEY-----\n" +
//            "MIIEpQIBAAKCAQEAux3iZQ1HcNtCjDdgyZ89EwHJJO0IlgHkV0K4+qo70X2fFgbF\n" +
//            "SksN6oxD2ecIcCGrJVzVoE6OkjUxtggU4HEV85xDRB9XjeIECUYRK5HkzFi7m0jx\n" +
//            "VtJDOhbO5dGxYZXH9qw0BeSMCiVcWLa+3itiEppynayNLeNj5CWlvA7d2E0Uxnik\n" +
//            "3mrWX4XuK0fjX0xK0NQsc+JywQ/PSw8l849XgG/scPB4QcTf1a7itV7WiJ2NXz8x\n" +
//            "3tLEWyND2Lv4eefLZlK5H7EUyfHFXKMDDo9HIH9RHGeI7QpS4KGcP/frZdSsplCX\n" +
//            "Im31CrEwHVuG3r00rohXUTuSFuyapC0GzjkM1QIDAQABAoIBAQCyPL452dNSallS\n" +
//            "rTGR/RA1Ob87YFqMYZL7mAASLfQ7Ejlmxt5JbYL9B81eFFeSUE/xkCOhxxLBrNAE\n" +
//            "gGA6hx4M5q2gh2ZkLYYAu3CBm0BhcqiKO4pAVJtbCvzcJ8malg1bKB+J+XF3/Kyh\n" +
//            "iLn5cUPDSsGg9gupSXAOk9nycArcDLcL1RA/8Oq8zD8DlNpCf/gv0e7QyJewh2cl\n" +
//            "l45wtkhsaCm4VYXquzvWenYKYwd/RPlIURCLqyNGdeIxikJtqlew7loHH8rGW/dt\n" +
//            "Xh1AZ1uc+ZyH0vEMEk0ZkfNBSkJO/6GVAb9Nd+E0aeWG5tCTA3bUDKmiOhjS44Yf\n" +
//            "FJo6cVXhAoGBAObuqHjwq6cfrIgc6IaZyBrqBnRjbjqstk30D8xjztfNe28pfCz5\n" +
//            "zgDpGwhdQi2mq8y/B5n52lZEOBb/uXAgVMy+2syJaBZSO+VwuEmg77mYTqzkhMpI\n" +
//            "VAaOBFqPbdh3D04eiW+t4i3C52GICniXHvQ8XlSzyz3gPYI0a+URoUtZAoGBAM9t\n" +
//            "pK2kCNDI5+dnadthyc640A/iQ95qTDY4ugWSeM7qTyc41X56eiz99rNH2yup6+kg\n" +
//            "zuzjc0kr+LFb31gH1YdR8CfHrgzZNTFaiZ6DOlIZDsRSVGxYN/j8q3M9C/SwNaaN\n" +
//            "1V2+po/hs+zmt0UmJsagM5+vE8+S2QjArSr5v+ndAoGAMkFBnIATet0juMNBvvj8\n" +
//            "WQ8QtIdm15H6YmB4h6w4uTHYaMqIYWlDcWNJJLrWygR5Yqc3ykRbRWa3Jhokisge\n" +
//            "bCgkiVplsmXW7wlmsW86fBMvG+TnGPZAguSpAeQwrWjLhd6TXiBjFj5SCCGI1dFd\n" +
//            "RR1WEezGkAKaoBk0WbdKeJECgYEAwOKGNenJQMzxyN8DMru2O4LkB0EyN0rFX85U\n" +
//            "PQY9TLvZqvFQpfIiVmA63e+9wNabxwd5JBNFiw9WjSaPg36VcuYg2fz2vn/k1OYJ\n" +
//            "/tqsitc+QhUE6FMIiYH7tIdZ7MlGuoA9npTAsh8BfxPz7FtTx3QFTUIyu90j8N2q\n" +
//            "qcHgUgkCgYEArOcjwD5r0zMFM5kZeZRf6QjPvmDzSjVCPxeEBqVaFwE/DeVtsYQ0\n" +
//            "T+agU1M7GQfjXJdmac7Joo2nan93Kus+SK31RJ/ie5dRxzrXAWW3owIpWw8ARrY2\n" +
//            "LgJ9cvL1gBigDgKOsxb4GxyLA5LAltK0JXX2KnlW2XGIcb9VBaQUDKE=\n" +
//            "-----END RSA PRIVATE KEY-----";
//
//    private String publicKey = "-----BEGIN PUBLIC KEY-----\n" +
//            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAux3iZQ1HcNtCjDdgyZ89\n" +
//            "EwHJJO0IlgHkV0K4+qo70X2fFgbFSksN6oxD2ecIcCGrJVzVoE6OkjUxtggU4HEV\n" +
//            "85xDRB9XjeIECUYRK5HkzFi7m0jxVtJDOhbO5dGxYZXH9qw0BeSMCiVcWLa+3iti\n" +
//            "EppynayNLeNj5CWlvA7d2E0Uxnik3mrWX4XuK0fjX0xK0NQsc+JywQ/PSw8l849X\n" +
//            "gG/scPB4QcTf1a7itV7WiJ2NXz8x3tLEWyND2Lv4eefLZlK5H7EUyfHFXKMDDo9H\n" +
//            "IH9RHGeI7QpS4KGcP/frZdSsplCXIm31CrEwHVuG3r00rohXUTuSFuyapC0GzjkM\n" +
//            "1QIDAQAB\n" +
//            "-----END PUBLIC KEY-----";
//
//    @Bean
//    public TokenStore tokenStore() {
//        return new JwtTokenStore(accessTokenConverter());
//    }
//
//    @Bean
//    public JwtAccessTokenConverter accessTokenConverter() {
//        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
//        converter.setSigningKey(privateKey);
//        converter.setVerifierKey(publicKey);
//        return converter;
//    }
//
//    @Bean
//    @Primary
//    public DefaultTokenServices tokenServices() {
//        DefaultTokenServices tokenServices = new DefaultTokenServices();
//        tokenServices.setTokenStore(tokenStore());
//        tokenServices.setSupportRefreshToken(true);
//        return tokenServices;
//    }
//    @Bean
//    public TokenEnhancer jwtTokenEnhancer() {
//        return new JwtTokenEnhancer();
//    }
//
//}
