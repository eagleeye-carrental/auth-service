package com.eagleeye.authservice.service;

import com.eagleeye.authservice.entity.Role;
import com.eagleeye.authservice.entity.UserEntity;

import java.util.Optional;

public interface RoleService {
    Role insertRole(UserEntity entity);
    Optional<Role> findByName(String name);
}
