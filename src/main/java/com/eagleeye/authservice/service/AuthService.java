package com.eagleeye.authservice.service;

import com.eagleeye.authservice.entity.UserEntity;
import com.eagleeye.authservice.model.OauthClientDetailDto;
import commonentity.user.UserDto;
import commonentity.user.UserRegisterDto;

public interface AuthService {

    OauthClientDetailDto saveOauthClient(OauthClientDetailDto clientDetails);
    UserDto register (UserRegisterDto userLoginDetails);
}
