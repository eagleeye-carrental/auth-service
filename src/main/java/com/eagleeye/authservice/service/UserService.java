package com.eagleeye.authservice.service;

import com.eagleeye.authservice.entity.UserEntity;
import commonentity.user.UserDto;

import java.util.Optional;

public interface UserService {
    Optional<UserEntity> findByEmail(String email);
    Optional<UserEntity> findByMobileNumber(String mobileNumber);
    Optional<UserEntity> findByUserName(String userName);
}
