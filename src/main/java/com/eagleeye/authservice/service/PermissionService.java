package com.eagleeye.authservice.service;

import com.eagleeye.authservice.entity.Permission;
import com.eagleeye.authservice.entity.Role;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
public interface PermissionService {
    Permission insertPermission(Role role);
    Optional<Permission> findByName(String name);

}
