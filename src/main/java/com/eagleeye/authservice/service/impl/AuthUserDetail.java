package com.eagleeye.authservice.service.impl;


import com.eagleeye.authservice.entity.UserEntity;
import com.eagleeye.authservice.repository.RoleRepository;
import com.eagleeye.authservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class AuthUserDetail extends UserEntity implements UserDetails {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;

    public AuthUserDetail() {
    }

    public AuthUserDetail(UserEntity userEntity) {
        super(userEntity);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authority = new ArrayList<>();
        getRoles().forEach(role -> {
            authority.add(new SimpleGrantedAuthority(role.getName()));
            role.getPermission().forEach(permisstion -> {
               authority.add(new SimpleGrantedAuthority(permisstion.getName()));
            });
        });
        return authority;

//        List<GrantedAuthority> authority = new ArrayList<>();
//        UserEntity user = userRepository.findById(Long.toString(getId()));
//        getRoles().forEach(role -> {
//            authority.add(new SimpleGrantedAuthority(role.getName()));
//        });
//        return authority;
    }
    @Override
    public String getPassword() {
        return super.getPassword();
    }

    @Override
    public String getUsername() {

        return super.getUserName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return super.isAccountNonExpired();
    }

    @Override
    public boolean isAccountNonLocked() {
        return super.isAccountNonLocked();
    }

    @Override
    public boolean isCredentialsNonExpired() {

        return super.isCredentialsNonExpired();
    }

    @Override
    public boolean isEnabled() {

        return super.isEnable();
    }


}
