package com.eagleeye.authservice.service.impl;

import com.eagleeye.authservice.entity.Permission;
import com.eagleeye.authservice.entity.Role;
import com.eagleeye.authservice.repository.PermissionRepository;
import com.eagleeye.authservice.service.PermissionService;
import com.netflix.discovery.converters.Auto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PermissionServiceImpl implements PermissionService {
    private final PermissionRepository permissionRepository;

    @Autowired
    public PermissionServiceImpl(PermissionRepository permissionRepository) {
        this.permissionRepository = permissionRepository;
    }

    @Override
    public Permission insertPermission(Role role) {

        Permission permission = new Permission();
        permission.setName("READ,WRITE,UPDATE,DELETE");
        permission.setRole(role);
        permissionRepository.save(permission);
        return permission;
    }


    @Override
    public Optional<Permission> findByName(String name) {
        return Optional.empty();
    }
}
