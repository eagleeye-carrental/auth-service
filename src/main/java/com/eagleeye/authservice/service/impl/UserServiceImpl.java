package com.eagleeye.authservice.service.impl;

import com.eagleeye.authservice.entity.UserEntity;
import com.eagleeye.authservice.repository.PermissionRepository;
import com.eagleeye.authservice.repository.RoleRepository;
import com.eagleeye.authservice.repository.UserRepository;
import com.eagleeye.authservice.service.UserService;
import commonentity.user.UserDto;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
public class UserServiceImpl implements UserService {
    private final ModelMapper modelMapper;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PermissionRepository permissionRepository;

    public UserServiceImpl(ModelMapper modelMapper, UserRepository userRepository, RoleRepository roleRepository, PermissionRepository permissionRepository) {
        this.modelMapper = modelMapper;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.permissionRepository = permissionRepository;
    }

    @Override
    public Optional<UserEntity> findByEmail(String email) {
        Optional<UserEntity> user = userRepository.findByEmail(email);
        if(user.isPresent()) {
            return user;
        }
        return Optional.empty();
    }

    @Override
    public Optional<UserEntity> findByMobileNumber(String mobileNumber) {

        Optional<UserEntity> user = userRepository.findByMobileNumber(mobileNumber);
        if(user.isPresent()) {
            return user;
        }
        return Optional.empty();
    }

    @Override
    public Optional<UserEntity> findByUserName(String userName) {
        return Optional.empty();
    }
}
