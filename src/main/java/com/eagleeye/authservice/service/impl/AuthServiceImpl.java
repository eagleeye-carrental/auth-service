package com.eagleeye.authservice.service.impl;

import com.eagleeye.authservice.entity.OauthClientDetails;
import com.eagleeye.authservice.entity.Role;
import com.eagleeye.authservice.entity.UserEntity;
import com.eagleeye.authservice.mapper.impl.UserMapper;
import com.eagleeye.authservice.model.OauthClientDetailDto;
import com.eagleeye.authservice.repository.OauthClientDetailsRepository;
import com.eagleeye.authservice.repository.UserRepository;
import com.eagleeye.authservice.service.AuthService;
import com.eagleeye.authservice.service.PermissionService;
import com.eagleeye.authservice.service.RoleService;
import commonentity.responseDto.RestResponse;
import commonentity.user.UserDto;
import commonentity.user.UserRegisterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

@Service
public class AuthServiceImpl implements AuthService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final RoleService roleService;
    private final PermissionService permissionService;
    private final OauthClientDetailsRepository oauthClientDetailsRepository;
    private final RestTemplate restTemplate;
    private final UserMapper userMapper;


    @Autowired
    public AuthServiceImpl(UserRepository userRepository,
                           PasswordEncoder passwordEncoder,
                           RoleService roleService,
                           PermissionService permissionService,
                           OauthClientDetailsRepository oauthClientDetailsRepository,
                           RestTemplate restTemplate,
                           UserMapper userMapper) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleService = roleService;
        this.permissionService = permissionService;
        this.oauthClientDetailsRepository = oauthClientDetailsRepository;
        this.restTemplate = restTemplate;
        this.userMapper = userMapper;
    }

    @Override
    public OauthClientDetailDto saveOauthClient(OauthClientDetailDto clientDetails) {
        return null;
    }

    @Transactional
    @Override
    public UserDto register(UserRegisterDto dto) {
        UserEntity user = saveUser(dto);
        UserDto userDto = mapper(dto, user);
//todo:: user register on user-service microservice
        RestResponse restResponse = restTemplate.postForObject("http://user/user/register", userDto, RestResponse.class);

        Role role = roleService.insertRole(user);
        permissionService.insertPermission(role);

        defaultOauthClientDetails(dto.getEmail());
        return userDto;
    }

    private UserDto mapper(UserRegisterDto dto, UserEntity user) {
        UserDto userDto = userMapper.convertToDto(user);
        userDto.setFirstName(dto.getFirstName());
        userDto.setMiddleName(dto.getMiddleName());
        userDto.setLastName(dto.getLastName());
        return userDto;
    }

    private UserEntity saveUser(UserRegisterDto dto) {
        UserEntity user = new UserEntity();
        user.setUserName(dto.getEmail());
        user.setPassword(passwordEncoder.encode(dto.getPassword()));
        user.setEmail(dto.getEmail());
        user.setMobileNumber(dto.getMobileNumber());
        user.setAccountNonExpired(true);
        user.setAccountNonLocked(true);
        user.setCredentialsNonExpired(true);
        user.setEnable(true);
        user.setVerificationCode("");
        user.setVerified(false);
        userRepository.save(user);
        return user;
    }

    private void defaultOauthClientDetails(String clientId) {

        if (!oauthClientDetailsRepository.findByClientId(clientId).isPresent()) {
            OauthClientDetails clientDetails = new OauthClientDetails();
            clientDetails.setClientId(clientId);
            clientDetails.setClientSecret(passwordEncoder.encode("pin"));
            clientDetails.setWebServerRedirectUri("http://localhost:8080/login");
            clientDetails.setScope("web,mobile");
            clientDetails.setAccessTokenValidity(3600);
            clientDetails.setRefreshTokenValidity(1000);
            clientDetails.setResourceIds("inventory,payment");
            clientDetails.setAuthorizedGrantTypes("authorization_code,password,refresh_token,implicit");
            clientDetails.setAuthorities("");
            clientDetails.setAutoApprove("");
            clientDetails.setAdditionalInformation("{}");
            //clientDetails.setAdditionalInformation(Collections.<String, Object>singletonMap("foo", Arrays.asList("rab")));
            oauthClientDetailsRepository.save(clientDetails);
        }
    }
}
