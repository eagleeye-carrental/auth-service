package com.eagleeye.authservice.service.impl;

import com.eagleeye.authservice.entity.Role;
import com.eagleeye.authservice.entity.UserEntity;
import com.eagleeye.authservice.repository.RoleRepository;
import com.eagleeye.authservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("customUserDetailService")
public class CustomUserDetailsService implements UserDetailsService {
    private UserRepository userRepositoty;
    private RoleRepository roleRepository;

    @Autowired
    public CustomUserDetailsService(UserRepository userRepositoty, RoleRepository roleRepository) {
        this.userRepositoty = userRepositoty;
        this.roleRepository = roleRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<UserEntity> user = userRepositoty.findByUserName(username);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        user.orElseThrow(()-> new UsernameNotFoundException("Username or password wrong"));

        /**
         * spring create userDetails
         * we create User
         * spring need spring details; need to convert
         */
        UserDetails userDetails = new AuthUserDetail(user.get());
        //check user status
        AccountStatusUserDetailsChecker accountStatusUserDetailsChecker = new AccountStatusUserDetailsChecker();
        accountStatusUserDetailsChecker.check(userDetails);
        return userDetails;
    }
}
