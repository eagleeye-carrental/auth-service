package com.eagleeye.authservice.service.impl;

import com.eagleeye.authservice.entity.Role;
import com.eagleeye.authservice.entity.UserEntity;
import com.eagleeye.authservice.repository.RoleRepository;
import com.eagleeye.authservice.service.RoleService;
import com.netflix.discovery.converters.Auto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
public class RoleServiceImpl implements RoleService {
    private final RoleRepository roleRepository;

    @Autowired
    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public Role insertRole(UserEntity entity) {
        Role role = null;
        if (!roleRepository.findByName("ROLE_user").isPresent()) {
            role = new Role();
            role.setName("ROLE_USER");
            role.setUserEntity(entity);
            role = roleRepository.save(role);
        }
        return role;
    }

    @Override
    public Optional<Role> findByName(String name) {
        return Optional.empty();
    }
}
