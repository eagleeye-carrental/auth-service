package com.eagleeye.authservice.startup;
import com.eagleeye.authservice.entity.OauthClientDetails;
import com.eagleeye.authservice.entity.Permission;
import com.eagleeye.authservice.entity.Role;
import com.eagleeye.authservice.entity.UserEntity;
import com.eagleeye.authservice.repository.OauthClientDetailsRepository;
import com.eagleeye.authservice.repository.PermissionRepository;
import com.eagleeye.authservice.repository.RoleRepository;
import com.eagleeye.authservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class CreateUser {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private OauthClientDetailsRepository clientDetailsRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PermissionRepository permissionRepository;


    @PostConstruct
    public void createUser() {

        systemUser();
        systemoauthClientDetails();


    }

    private void systemoauthClientDetails() {
        OauthClientDetails clientDetails = new OauthClientDetails();
        String clientId = "mobile";
        clientDetails.setClientId(clientId);
        clientDetails.setClientSecret(passwordEncoder.encode("pin"));
        clientDetails.setWebServerRedirectUri("http://localhost:8080/login");
        clientDetails.setScope("web,mobile");
        clientDetails.setAccessTokenValidity(3600);
        clientDetails.setRefreshTokenValidity(1000);
        clientDetails.setResourceIds("inventory,payment");
        clientDetails.setAuthorizedGrantTypes("authorization_code,password,refresh_token,implicit");
        clientDetails.setAuthorities("");
        clientDetails.setAutoApprove("");
        clientDetails.setAdditionalInformation("{}");
        //clientDetails.setAdditionalInformation(Collections.<String, Object>singletonMap("foo", Arrays.asList("rab")));

        if (!clientDetailsRepository.findByClientId(clientId).isPresent()) {
            clientDetailsRepository.save(clientDetails);
        }

    }

    private void systemUser() {
        UserEntity user = new UserEntity();
        String username = "rajesh@gmail.com";
        user.setUserName(username);
        user.setPassword(passwordEncoder.encode("password"));
        user.setEmail("rajesh@gmail.com");
        user.setMobileNumber("0426402214");
        user.setAccountNonExpired(true);
        user.setAccountNonLocked(true);
        user.setCredentialsNonExpired(true);
        user.setEnable(true);
        user.setVerificationCode("");
        //

        if (!userRepository.findByUserName(username).isPresent()) {
            user = userRepository.save(user);
            createRole(user);

        }
    }

    private Role createRole(UserEntity userEntity) {
        Role role = null;
        if (!roleRepository.findByName("ROLE_admin").isPresent()) {
            role = new Role();
            role.setName("ROLE_admin");
            role.setUserEntity(userEntity);
            role = roleRepository.save(role);
            createPermission(role);
        }
        return role;
    }

    private Permission createPermission(Role role) {
        Permission permission = null;
        if (
                (!permissionRepository.findByName("READ,WRITE").isPresent())
                        &&
                        (!permissionRepository.findByRole(role).isPresent())) {
            permission = new Permission();
            permission.setName("READ,WRITE,DELETE,UPDATE");
            permission.setRole(role);
            permissionRepository.save(permission);
        }
        return permission;
    }


}
