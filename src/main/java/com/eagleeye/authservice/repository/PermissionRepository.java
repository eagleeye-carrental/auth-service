package com.eagleeye.authservice.repository;

import com.eagleeye.authservice.entity.Permission;
import com.eagleeye.authservice.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface PermissionRepository extends JpaRepository<Permission, Long> {
    @Query("select p FROM Permission p WHERE p.name=?1")
    Optional<Permission> findByName(String permission);
    @Query("select p FROM Permission p WHERE p.role=?1")
    Optional<Permission> findByRole(Role role);
}
