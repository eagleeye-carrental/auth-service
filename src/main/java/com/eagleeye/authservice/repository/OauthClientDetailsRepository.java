package com.eagleeye.authservice.repository;

import com.eagleeye.authservice.entity.OauthClientDetails;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface OauthClientDetailsRepository extends JpaRepository<OauthClientDetails,String> {

    Optional<OauthClientDetails> findByClientId(String clientId);
}
