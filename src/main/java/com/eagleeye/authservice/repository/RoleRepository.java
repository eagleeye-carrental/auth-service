package com.eagleeye.authservice.repository;

import com.eagleeye.authservice.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role,Long> {

    @Query("SELECT r from Role r WHERE r.name=?1")
    Optional<Role> findByName(String role);
}
