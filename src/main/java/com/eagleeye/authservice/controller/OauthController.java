package com.eagleeye.authservice.controller;

import com.eagleeye.authservice.Error.UserError;
import com.eagleeye.authservice.entity.UserEntity;
import com.eagleeye.authservice.service.AuthService;
import com.eagleeye.authservice.service.UserService;
import com.eagleeye.authservice.validation.UserCustomValidation;
import commonentity.responseDto.RestResponse;
import commonentity.user.UserDto;
import commonentity.user.UserRegisterDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.ws.rs.core.SecurityContext;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@RestController
public class OauthController {
    private final Logger logger = LoggerFactory.getLogger(OauthController.class);
    private final UserCustomValidation uservalidation;
    private final AuthService authService;

    @Autowired
    public OauthController(UserCustomValidation uservalidation, AuthService authService) {
        this.uservalidation = uservalidation;
        this.authService = authService;
    }

    @PostMapping("/user/register")
    public ResponseEntity<RestResponse> register(@Valid @RequestBody UserRegisterDto registerDto) {
        RestResponse restResponse = null;
        try {
            UserError validation = uservalidation.validation(registerDto);
            if (validation.isValid()) {
                UserDto user = authService.register(registerDto);
                restResponse = new RestResponse("User Saved", user);
                return new ResponseEntity<>(restResponse, HttpStatus.CREATED);
            } else {
                restResponse = new RestResponse("Validation Failed!", validation);
                return new ResponseEntity<>(restResponse, HttpStatus.CONFLICT);
            }
        } catch (Exception e) {
            logger.debug("Exception occurred due to "+ e);
            restResponse = new RestResponse("Exception", null);
            return new ResponseEntity<>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @GetMapping("/user/info")
    public Map<String,Object> lskdfj(OAuth2Authentication user) {
        Map<String,Object> userInfo = new HashMap<>();
        userInfo.put("user",user.getUserAuthentication().getPrincipal());
        return userInfo;
    }

}
