package com.eagleeye.authservice.Error;

import lombok.Data;

import java.io.Serializable;
@Data
public class UserError implements Serializable {
    private String email;
    private String mobileNumber;
    private String re_password;
    private boolean valid;
}
