package com.eagleeye.authservice.validation;

import com.eagleeye.authservice.Error.UserError;
import com.eagleeye.authservice.service.AuthService;
import com.eagleeye.authservice.service.UserService;
import commonentity.user.UserRegisterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserCustomValidation {
    private final AuthService authService;
    private final UserService userService;

    private UserError error;

    @Autowired
    public UserCustomValidation(AuthService authService, UserService userService) {
        this.authService = authService;
        this.userService = userService;
    }

    public UserError validation(UserRegisterDto registerDto) {
        error = new UserError();
        boolean valid = true;
        valid = checkEmail(registerDto.getEmail()) && valid;
        valid = checkMobileNumber(registerDto.getMobileNumber()) && valid;
        valid = checkPassword(registerDto) && valid;
        //todo:: check by username
        error.setValid(valid);
        return error;

    }

    private boolean checkPassword(UserRegisterDto registerDto) {
        if (!registerDto.getPassword().equals(registerDto.getRe_password())) {
            error.setRe_password("Password Incorrect!");
            return false;
        }
        return true;
    }

    private boolean checkMobileNumber(String mobileNumber) {
        if (userService.findByMobileNumber(mobileNumber).isPresent()) {
            error.setMobileNumber("Mobile Number already exists");
            return false;
        }
        return true;
    }

    private boolean checkEmail(String email) {
        if (userService.findByEmail(email).isPresent()) {
            error.setEmail("Email already exists");
            return false;
        }
        return true;
    }
}
