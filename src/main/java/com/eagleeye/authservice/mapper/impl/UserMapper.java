package com.eagleeye.authservice.mapper.impl;

import com.eagleeye.authservice.entity.UserEntity;
import com.eagleeye.authservice.mapper.iMapper;
import commonentity.user.UserDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserMapper implements iMapper<UserEntity, UserDto> {
    private final ModelMapper modelMapper;

    @Autowired
    public UserMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public UserEntity convertToEntity(UserDto dto) {
        UserEntity entity = new UserEntity();
        modelMapper.map(dto,entity);
        return entity;
    }

    @Override
    public UserDto convertToDto(UserEntity entity) {
        UserDto dto = new UserDto();
        modelMapper.map(entity,dto);
        return dto;
    }
}
