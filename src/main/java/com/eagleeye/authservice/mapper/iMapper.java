package com.eagleeye.authservice.mapper;

public interface iMapper <E,D>{
    E convertToEntity(D dto);
    D convertToDto(E entity);
}
