package com.eagleeye.authservice.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "user")
@Setter
@Getter
public class UserEntity implements Serializable {
    public UserEntity(){}

    public UserEntity(UserEntity userEntity) {
        this.id = userEntity.getId();
        this.userName = userEntity.getUserName();
        this.password   = userEntity.getPassword();
        this.email = userEntity.getEmail();
        this.enable = userEntity.isEnable();
        this.accountNonExpired = userEntity.isAccountNonExpired();
        this.accountNonLocked = userEntity.isAccountNonLocked();
        this.credentialsNonExpired = userEntity.isCredentialsNonExpired();
        this.roles = userEntity.getRoles();
        this.verificationCode = userEntity.getVerificationCode();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "username")
    private String userName;

    @JsonIgnore
    @Column(name = "password")
    private String password;

    @Column(name = "email")
    private String email;

    private String mobileNumber;

    @JsonIgnore
    @Column(name = "enabled")
    private boolean enable;

    @JsonIgnore
    @Column(name = "accountNonExpired")
    private boolean accountNonExpired;

    @JsonIgnore
    @Column(name = "accountNonLocked")
    private boolean accountNonLocked;

    @JsonIgnore
    @Column(name = "credentialsNonExpired")
    private boolean credentialsNonExpired;

    @JsonIgnore
    @Column(name = "verification_code", nullable = true)
    private String verificationCode;
    private boolean isVerified;

//    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//    @JoinTable(name = "role_user", joinColumns = {
//          @JoinColumn(name = "user_id", referencedColumnName = "id")
//    }, inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
//    private List<Role> roles;

    @JsonIgnore
    @OneToMany(mappedBy = "userEntity", fetch = FetchType.EAGER)
    List<Role> roles;
}
