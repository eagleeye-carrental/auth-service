package com.eagleeye.authservice.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "oauth_approvals")
@Data
public class OauthApprovals implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", columnDefinition = "bigint unsigned")
    private Integer id;

    @Column(name = "userId")
    private String userId;

    @Column(unique = true, name = "clientId")
    private String clientId;

    @Column(name = "scope")
    private String scope;

    @Column(name = "status")
    private String status;

    @Column(name = "expiresAt")
    private Date expiresAt;

    @Column(name = "lastModifiedAt")
    private Date lastModifiedAt;

}
